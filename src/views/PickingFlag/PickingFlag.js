import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';
import PropTypes, { func } from 'prop-types';
import MuiAlert from '@material-ui/lab/Alert';

import {
  MuiThemeProvider,
  createMuiTheme,
  TablePagination,
  Dialog,
  Button,
  CardActions,
  Checkbox,
  TextField,
  MenuItem,
  FormControl,
  FormHelperText,
  Select,
  InputLabel,
  Snackbar,
  CardContent,
  Card
} from '@material-ui/core';
import { validateNumeric, validateNumericNoDecimal } from '../../common/validators';
import MaterialTable from 'material-table';
import UserModel from '../../models/UserModel';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 800
  },
  statusContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  status: {
    marginRight: theme.spacing(1)
  },
  actions: {
    justifyContent: 'flex-end'
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const PickingFlag = props => {
  const [boolVal, setBoolVal] = useState('');
  const [isLoad, setIsLoad] = useState(false);
  const classes = useStyles();
  const [batches, setBatches] = useState([{}]);
  const [selectedBatch, setSelectedBatch] = useState({});
  const { className, ...rest } = props;
  const [openData, setOpenData] = useState({
    openSuccess: false,
    openError: false
  });
  var [data, setData] = useState([
    // {sku: "blabla", qtty:"blaqtty", wh_qtty:"wh qtty", picked_qtty:2, approve: true, delta:"12", adjusted:'',net_wh_qtty:10},
    // {sku: "blabla2", qtty:"blaqtty2", wh_qtty:"wh qtty2", picked_qtty:3, approve: false, delta:"12", adjusted:'',net_wh_qtty:4}
  ]);
  const columns = [
    { title: 'SKU', field: 'sku', editable: 'never' },
    {
      title: 'Order Quantity',
      field: 'qtty',
      editable: 'never',
      filtering: true
    },
    {
      title: 'Warehouse Quantity',
      field: 'wh_qtty',
      editable: 'never',
      filtering: false
    },
    {
      title: 'Picked Quantity',
      field: 'picked_qtty',
      editable: 'never',
      filtering: false
    },

    // {
    //   title: 'Delta',
    //   field: 'delta',
    //   editable: 'never',
    //   filtering: false,
    // },
    {
      title: 'Approve', field: 'approve', name: 'xyz', render: rowData => <Checkbox value={rowData.approve} checked={rowData.approve} disabled={rowData.is_picker ? false : true}
        onChange={async (evt, val) => {
          console.log('evt', val)
          let newData = {
            batch_id: rowData.batch_id,
            is_picker: rowData.is_picker,
            sku_id: rowData.sku_id,
            id: rowData.id,
            sku: rowData.sku,
            qtty: rowData.qtty,
            wh_qtty: rowData.wh_qtty,
            picked_qtty: rowData.picked_qtty,
            approve: val, delta: rowData.delta,
            adjusted: val ? rowData.picked_qtty : rowData.adjusted,
            net_wh_qtty: rowData.net_wh_qtty
          }
          console.log('new daaata', newData)
          const dataUpdate = [...data];
          const index = rowData.tableData.id;
          dataUpdate[index] = newData;
          setData([...dataUpdate]);
          // let newData =  {sku: rowData.sku, qtty:rowData.qtty, wh_qtty:rowData.wh_qtty, reqd_purchase_qtty:rowData.reqd_purchase_qtty, fulfilled: val, act_purchase:rowData.act_purchase, total_purchase_cost:rowData.total_purchase_cost} 
          // console.log('new daaata',newData)
          // const dataUpdate = [...data];
          // const index = rowData.tableData.id;
          // dataUpdate[index] = newData;
          // setData([...dataUpdate]);
          // rowData.fulfilled = val
          // let dat = {
          //     ...data[rowData.tableData.id],
          //     fulfilled: val
          // };
          // setData(dat)
          //console.log(data)
        }}
      />, filtering: false
    },

    // {sku: "blabla2", qtty:"blaqtty2", wh_qtty:"wh qtty2", picked_qtty:3, approve: false, delta:"12", adjusted:'',net_wh_qtty:4}

    {
      title: 'Adjusted Picked Quantity',
      field: 'adjusted',
      editable: 'never',
      filtering: false,
      render: rowData => (
        <TextField
          value={rowData.approve ? rowData.picked_qtty : rowData.adjusted}
          disabled={rowData.is_picker ? (rowData.approve ? true : false) : true}
          type='number'
          onChange={(event) => {
            if (validateNumericNoDecimal(parseFloat(event.target.value)) || !event.target.value) {
              if (parseInt(event.target.value) <= parseInt(rowData.wh_qtty) || event.target.value === "") {
                console.log('rowdata', rowData)
                const dataUpdate = [...data];
                const index = rowData.tableData.id;
                let newData = {
                  batch_id: rowData.batch_id,
                  is_picker: rowData.is_picker,
                  sku_id: rowData.sku_id,
                  id: rowData.id,
                  sku: rowData.sku,
                  qtty: rowData.qtty,
                  wh_qtty: rowData.wh_qtty,
                  picked_qtty: rowData.picked_qtty,
                  approve: rowData.approve,
                  // delta:  rowData.picked_qtty - rowData.qtty,
                  delta: rowData.delta,
                  adjusted: event.target.value,
                  net_wh_qtty: '',

                }
                dataUpdate[index] = newData
                setData([...dataUpdate])
              }
            }
          }
          }
        />
      )
    }
    // {
    //   title: 'Net Warehouse Quantity',
    //   field: 'net_wh_qtty',
    //   editable: 'never',
    //   filtering: false
    // }
  ];

  useEffect(() => {
    let tempArr = [];
    UserModel.getInstance().getBatchesManager(
      succ => {
        succ.forEach((batch) => {
          let arr = batch.batch_name.split("_");
          tempArr.push(
            { id: batch.id, batch_name: "batch " + batch.created_at }
          )
        })
        setBatches(tempArr)
      },
      err => {
        alert(err);
        console.log('err', err);
      }
    );
  }, []);

  //    {sku: "blabla", qtty:"blaqtty", wh_qtty:"wh qtty", picked_qtty:2, approve: true, delta:"12", adjusted:'',net_wh_qtty:10},

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenData({ openSuccess: false, openError: false });
  };


  const handleSubmit = () => {
    // console.log('handle submit',empty)
    console.log('procurement data', data);
    let isempty = false;
    setIsLoad(true);
    let submitData = [];
    data.forEach(obj => {
      if (obj.adjusted == '') {
        console.log('obj', obj);
        isempty = true;
      }

      submitData.push({
        id: obj.id,
        qty: obj.adjusted
        // price: obj.net_wh_qtty
      });
    });

    console.log('temp arr submit', submitData);
    console.log('data[o].id', data[0].id);
    if (isempty) {
      console.log('if');
      alert('plz make sure all fields are filled');
      setIsLoad(false);
      // setOpenData({ openError: true, openSuccess: false })
      //  setEmpty(false)
    } else {
      console.log('else', submitData);
      UserModel.getInstance().pickerConfirmation(
        { items: submitData },
        async succ => {
          setIsLoad(false);
          await setOpenData({ openSuccess: true });
          setData([]);
          setSelectedBatch({});
          console.log('succ data', succ);
          window.location.reload();
        },
        err => {
          setIsLoad(false);
          setOpenData({ openError: true, openSuccess: false });
          console.log('err', err);
        }
      );
    }
  };

  const handleChange = event => {
    setIsLoad(true);
    console.log('value', event.target.value);
    setSelectedBatch(event.target.value);
    let tempArr = [];
    UserModel.getInstance().getManagerSpecificBatch(
      event.target.value.id,
      async data => {
        console.log('data', data);
        await data.forEach(obj => {
          tempArr.push({
            batch_id: obj.batch_id,
            sku_id: obj.sku_id,
            id: obj.id,
            sku: obj.sku_name,
            qtty: obj.order_qty,
            wh_qtty: obj.stock,
            picked_qtty: obj.is_picker ? obj.picker_qty : 'not picked yet',
            approve: false,
            delta: obj.picker_qty ? obj.picker_qty - obj.order_qty : '',
            adjusted: obj.verify_picker_qty ? obj.verify_picker_qty : '',
            net_wh_qtty: '',
            is_picker: obj.is_picker
          });
        });
        setIsLoad(false);
        console.log('temp arr', tempArr);
        setData(tempArr);
      },
      err => {
        setIsLoad(false);
        console.log('err', err);
      }
    );
  };

  return (
    <div>
      <Card>
        <div>
          <FormControl className={classes.formControl}>
            <InputLabel id="demo-simple-select-helper-label">Batch</InputLabel>
            <Select
              labelId="demo-simple-select-helper-label"
              id="demo-simple-select-helper"
              value={selectedBatch}
              onChange={handleChange}>
              {batches.map(val => {
                return (
                  <MenuItem value={val}>
                    {new Date(val.created_at).toLocaleString()}
                  </MenuItem>
                );
              })}
            </Select>
            <FormHelperText style={{ margin: 10 }}>
              Select a Batch Id
            </FormHelperText>
          </FormControl>
        </div>
        <CardActions>
          <MaterialTable
            title="Picking Confirmation"
            columns={columns}
            data={data}
            isLoading={isLoad}
            className={clsx(classes.root, className)}
            // onFilterChange={filterChange}

            options={{
              paging: false,
              exportButton: true,
              filtering: true
              // selection: true,
            }}></MaterialTable>
        </CardActions>
        <CardContent>
          <Snackbar
            open={openData.openSuccess}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Picker Data Successfully updated
            </Alert>
          </Snackbar>
          <Snackbar
            open={openData.openError}
            autoHideDuration={3000}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              Error while updating Data
            </Alert>
          </Snackbar>
        </CardContent>
        <CardActions>
          <Button
            color="primary"
            variant="contained"
            disabled={selectedBatch.batch_name ? false : true}
            onClick={handleSubmit}>
            Confirm Picking
          </Button>
        </CardActions>
      </Card>
    </div>
  );
};

PickingFlag.propTypes = {
  className: PropTypes.string
};

export default PickingFlag;
